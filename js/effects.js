$(document).ready(function(){

var dataPoints = [
     {
         value: 1820,
         color: "rgba(44,195,218,1)"
     },
     {
         value: 800,
         color: "rgba(44,195,218,0.8)"
     },
     {
         value: 1100,
         color: "rgba(44,195,218,0.6)"
     },
     {
         value: 1720,
         color: "rgba(44,195,218,0.4)"
     },
     {
         value: 1140,
         color: "rgba(44,195,218,0.2)"
     }
 ];

 window.onload = function(){
     var ctx = document.getElementById("chart-area").getContext("2d");
     window.myPolarArea = new Chart(ctx).PolarArea(dataPoints, {
         responsive:true
     });
     // window.myDoughnutChart = new Chart(ctx).Doughnut(dataPoints, {
     //     responsive:true
     // });
 };

});
