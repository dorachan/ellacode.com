module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-haml');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({
        haml: {
            dist: {
                files: {
                    'index.html': 'index.haml'
                }
            }
        },
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'stylesheets',
                    src: ['*.scss'],
                    dest: 'stylesheets',
                    ext: '.css'
                }]
            }
        },
        watch: {
            haml: {
                files: ['**/*.haml'],
                tasks: ['haml']
            },
            sass: {
                files: ['**/*.scss'],
                tasks: ['sass']
            }
        }
    });

    grunt.registerTask('default', ['watch']);
};
